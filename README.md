# QA Automation Demo

Made with Python using the [Selenium](https://www.seleniumhq.org/) framework

## Description

###### GoogleTestFirst.py

The script open the browser, checks the search input, search button and I'm feeling Lucky button to exist

###### GoogleTestSecond.py

The script opens a browser, searches for a term and checks the second search result

###### GoogleTestThird.py

The script opens a browser, writes something in the search input, checks the suggestion box has appeared then checks all the suggestions and makes sure the initial term is present

###### GoogleTestFourth.py

The script searches for a term, clicks on the Next button, checks the second page of results has been loaded, clicks on Previous and checks if the first page of results has been loaded

###### GoogleTestFifth.py

The script searches for a term, checks the search input is present on the page, scrolls down and checks the search input to still be visible